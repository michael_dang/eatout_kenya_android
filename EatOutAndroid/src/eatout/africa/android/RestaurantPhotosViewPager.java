package eatout.africa.android;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class RestaurantPhotosViewPager extends ViewPager implements OnTouchListener {
    private boolean pagerMoved = false;
    static final long ANIM_VIEWPAGER_DELAY = 3500;
    final Handler h;
    Runnable animateViewPager;

	private static final int NUM_PAGES = 3;
	
	public RestaurantPhotosViewPager(Context context) {
		super(context);
        h = new Handler();
        animateViewPager = new Runnable() {
            public void run() {
                if (!pagerMoved) {
                	int nextItem = getCurrentItem() == NUM_PAGES-1 ? 0 : getCurrentItem()+1;
                    setCurrentItem(nextItem, true);
                    h.postDelayed(this, ANIM_VIEWPAGER_DELAY);
                }
            }
        };
        //animateViewPager.run();
		
		// TODO Auto-generated constructor stub
	}

	public RestaurantPhotosViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
        h = new Handler();
        animateViewPager = new Runnable() {
            public void run() {
                if (!pagerMoved) {
                	int nextItem = getCurrentItem() == NUM_PAGES-1 ? 0 : getCurrentItem()+1;
                    setCurrentItem(nextItem, true);
                    h.postDelayed(this, ANIM_VIEWPAGER_DELAY);
                }
            }
        };
        animateViewPager.run();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent touch) {
		// TODO Auto-generated method stub
		if(touch.getAction() == MotionEvent.ACTION_DOWN)
			pagerMoved = true;
		else if(touch.getAction() == MotionEvent.ACTION_UP)
			pagerMoved = false;
		return true;
	}

}
