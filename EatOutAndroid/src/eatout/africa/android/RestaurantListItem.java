package eatout.africa.android;

public class RestaurantListItem {
	public final static int GOLD_RATING = 2;
	public final static int SILVER_RATING = 1;
	public final static int BRONZE_RATING = 0;
	
	String restaurantName;
	String restaurantAddress;
	String[] restaurantCuisine;
	int restaurantRating;
	int restaurantId;
	
	public RestaurantListItem(String name, String address, String[] cuisine, int rating, int id){
		restaurantName = name;
		restaurantAddress = address;
		restaurantCuisine = cuisine;
		restaurantRating = rating;
		restaurantId = id;
	}
	
	public String getName(){
		return restaurantName;
	}
	
	public String getAddress(){
		return restaurantAddress;
	}
	
	public String[] getCuisine(){
		return restaurantCuisine;
	}
	
	public int getRating(){
		return restaurantRating;
	}
	
	public long getRestaurantId(){
		return restaurantId;
	}

}
