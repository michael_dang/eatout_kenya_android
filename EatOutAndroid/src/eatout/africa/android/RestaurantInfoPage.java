package eatout.africa.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ActionBar.LayoutParams;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class RestaurantInfoPage extends FragmentActivity {
	private final int KENYA_DEFAULT_COUNTRY = 3;
	private final String COUNTRY_PREFERENCES = "country preferences";
	private final String COUNTRY_POSITION = "country position";
	private int NUM_PAGES = 3;
	
	private GoogleMap map;
	private static double lat;
	private static double lng;
	private RestaurantPhotosViewPager photosViewPager;
	private PagerAdapter photosPagerAdapter;
	private RestaurantEntry thisRestaurant;
	private LinearLayout buttonLayout;
	private ImageButton homeButton;
	private Button countryButton;
	private Button callRestaurantButton;
	private Button IButton;
	private Button M1Button;
	private Button PButton;
	private Button RButton;
	private Button M2Button;
	private Spinner countrySpinner;
	private ToHomePageCountrySpinnerOISL countryOISL;
	private SharedPreferences countryPrefs;
	private ViewFlipper buttonFlipper;
	private Uri phoneNumberUri;
	private String phoneNumber = "0711-222-222";
    private WebView infoPage;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restaurant_info_page);
		GetRestaurant getRes = new GetRestaurant();
		getRes.execute();
        countryPrefs = getSharedPreferences(COUNTRY_PREFERENCES, 0);
        buttonLayout =(LinearLayout) findViewById(R.id.five_button_layout);
        countryButton = (Button) findViewById(R.id.country_button);
        callRestaurantButton = (Button) findViewById(R.id.call_restaurant);
        homeButton = (ImageButton) findViewById(R.id.home_button);
        IButton = (Button) findViewById(R.id.I_button);
        M1Button = (Button) findViewById(R.id.M1_button);
        PButton = (Button) findViewById(R.id.P_button);
        RButton = (Button) findViewById(R.id.R_button);
        M2Button = (Button) findViewById(R.id.M2_button);
        phoneNumberUri = Uri.parse("tel:" + phoneNumber);
        countrySpinner = (Spinner) findViewById(R.id.country_spinner);
        buttonFlipper = (ViewFlipper) findViewById(R.id.button_view_flipper);
        infoPage = (WebView) findViewById(R.id.info_page);
        Fragment d = getSupportFragmentManager().findFragmentById(R.id.map);
        map = ((SupportMapFragment) d).getMap();
        if(map == null)
        	Toast.makeText(getApplicationContext(), "Unable to display maps", Toast.LENGTH_SHORT).show();
        
        photosViewPager = (RestaurantPhotosViewPager) findViewById(R.id.photos);
        photosPagerAdapter = new PhotosScreenSlidePagerAdapter(getSupportFragmentManager());
        photosViewPager.setAdapter(photosPagerAdapter);
        
        homeButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {

				// TODO Auto-generated method stub
		    	new AlertDialog.Builder(RestaurantInfoPage.this)
		    	.setMessage("Go back to the home page?")
		    	.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    	    public void onClick(DialogInterface dialog, int whichButton) {
						Intent toHomePage = new Intent(RestaurantInfoPage.this, HomePage.class);
						startActivity(toHomePage);
		    	    }})
		    	 .setNegativeButton(android.R.string.no, null).show();
			}
        	
        });

        OnClickListener buttonListener = new OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                case R.id.I_button:
                    buttonFlipper.setDisplayedChild(0);
                    break;
                case R.id.M1_button:
                	buttonFlipper.setDisplayedChild(1);
        			CameraPosition cameraPos = new CameraPosition.Builder().target(new LatLng(lat,lng)).zoom(12).build();
        	        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos));
                    break;
                case R.id.P_button:
                	buttonFlipper.setDisplayedChild(2);
            		photosViewPager.h.removeCallbacks(photosViewPager.animateViewPager);
            		photosViewPager.h.postDelayed(photosViewPager.animateViewPager, photosViewPager.ANIM_VIEWPAGER_DELAY);
                    break;
                case R.id.R_button:
                	buttonFlipper.setDisplayedChild(3);
                    break;
                case R.id.M2_button:
                	buttonFlipper.setDisplayedChild(4);
                    break;
                }
            }
        };
        
        IButton.setOnClickListener(buttonListener);
        M1Button.setOnClickListener(buttonListener);
        PButton.setOnClickListener(buttonListener);
        RButton.setOnClickListener(buttonListener);
        M2Button.setOnClickListener(buttonListener);
        
        
        int countryPos = countryPrefs.getInt(COUNTRY_POSITION, KENYA_DEFAULT_COUNTRY);
        final String[] countryAbbrev = getResources().getStringArray(R.array.country_abbrev);
        final String[] countryNames = getResources().getStringArray(R.array.country_entries);

        countrySpinner.setSelection(countryPos);
        countryButton.setText(countryAbbrev[countryPos]);
        countrySpinner.setOnItemSelectedListener(new ToHomePageCountrySpinnerOISL(getBaseContext(), countryAbbrev, countryNames, countryButton));
        countryButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {

				new AlertDialog.Builder(RestaurantInfoPage.this)
		    	.setMessage("Do you want to switch countries?")
		    	.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    	    public void onClick(DialogInterface dialog, int whichButton) {
		    	        countrySpinner.performClick();
		    	    }})
		    	 .setNegativeButton(android.R.string.no, null).show();
			}
        });
        
        callRestaurantButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
		    	new AlertDialog.Builder(RestaurantInfoPage.this)
		    	.setTitle("Call Restaurant")
		    	.setMessage("Do you want to make the call?")
		    	.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    	    public void onClick(DialogInterface dialog, int whichButton) {

						Intent phoneActivity = new Intent(Intent.ACTION_DIAL);
						phoneActivity.setData(phoneNumberUri);
						startActivity(phoneActivity);
						
		    	    }})
		    	 .setNegativeButton(android.R.string.no, null).show();
			}
        });
        
        
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
        setButtonHeight();

		super.onWindowFocusChanged(hasFocus);
	}

	private void setButtonHeight(){
		int width = buttonLayout.getMeasuredWidth();
		width = width/5;
		LinearLayout.LayoutParams adjustHeight = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                width);
		
		buttonLayout.setLayoutParams(adjustHeight);
		IButton.setHeight(width);
		M1Button.setHeight(width);
		PButton.setHeight(width);
		RButton.setHeight(width);
		M2Button.setHeight(width);
		
		
		
		M2Button.setWidth(M2Button.getMeasuredWidth()+1);
		
	}
	/*
	private void setMapLocation(){
		mapView.setSatellite(false);
		mapView.setTraffic(false);
		mapView.setBuiltInZoomControls(true);
		
		int maxZoom = mapView.getMaxZoomLevel();
		int initZoom = maxZoom-2;
		mapControl = mapView.getController();
		mapControl.setZoom(initZoom);
		lat = thisRestaurant.getLatitude();
		lon = thisRestaurant.getLongitude();
		latInt = (int) (lat*1e6);
		lonInt = (int) (lon*1e6);
		gp = new GeoPoint(latInt, lonInt);
		
		mapControl.animateTo(gp);
	}*/
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		photosViewPager.h.removeCallbacks(photosViewPager.animateViewPager);

		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.restaurant_info_page, menu);
		return true;
	}
	
    private class PhotosScreenSlidePagerAdapter extends FragmentStatePagerAdapter implements OnGestureListener {
        public PhotosScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	if(position >= NUM_PAGES)
        		position = 0;
        	
            return PhotosScreenSlideFragment.create(position);
        }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return NUM_PAGES;
		}

		@Override
		public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
				float arg3) {
			// TODO Auto-generated method stub
			onFling(arg0, arg1, arg2, arg3);

			return true;
		}

		@Override
		public boolean onDown(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
				float arg3) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onLongPress(MotionEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onShowPress(MotionEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean onSingleTapUp(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return false;
		}

    }
/*
    private class MapViewActivity extends MapActivity{

		@Override
		protected void onCreate(Bundle arg0) {
			// TODO Auto-generated method stub
			super.onCreate(arg0);
	        mapView = (MapView) findViewById(R.id.map);

	        setMapLocation();
		}

		@Override
		protected void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
		}

		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
		}

		@Override
		protected boolean isRouteDisplayed() {
			// TODO Auto-generated method stub
			return false;
		}
    	
    	
    }*/
    
	private class GetRestaurant extends AsyncTask<Void, String, Void>{

		int restaurantId;
		DatabaseHandler db;

		@Override
		protected void onCancelled(Void result) {
			// TODO Auto-generated method stub
			super.onCancelled(result);
			db.close();

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			db.close();
			
			//MapViewActivity mapViewActivity = new MapViewActivity();
			//mapViewActivity.
			lat = thisRestaurant.getLatitude();
			lng = thisRestaurant.getLongitude();

	        MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lng)).title(thisRestaurant.getDisplayName());
	        map.addMarker(marker);
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			Intent restaurantIntent = getIntent();
			restaurantId = restaurantIntent.getIntExtra("restaurant Id", -1);
			db = new DatabaseHandler(getBaseContext());
			try {
				db.makeDatabase();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			if(values[0] == null)
				Toast.makeText(getBaseContext(), "No restaurant exists", Toast.LENGTH_SHORT);
			else {
				infoPage.loadData(thisRestaurant.getHTML(),"text/html", null);


			}
			super.onProgressUpdate(values);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			db.openDataBase();
			thisRestaurant = db.getRestaurant(restaurantId);

			publishProgress(thisRestaurant.getHTML());
			
			
			
			
			HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost("http://www.eatout.co.ke/api/20131024/restaurant/getRestaurantDetails.php");
	        HttpResponse response = null;
	        
		    try {

		        List<NameValuePair> pairsRestaurantDetails = new ArrayList<NameValuePair>(4);
		        pairsRestaurantDetails.add(new BasicNameValuePair("login", "android"));
		        pairsRestaurantDetails.add(new BasicNameValuePair("password", "dangmichael"));
		        pairsRestaurantDetails.add(new BasicNameValuePair("restaurantId", String.valueOf(restaurantId)));
		        pairsRestaurantDetails.add(new BasicNameValuePair("version", ""));
		        
		        //String url = "http://www.eatout.co.ke/api/20131024/restaurant/getRestaurantsByCity.php";
		        //URI uri = new URI( url + "?" + URLEncodedUtils.format( nameValuePairs, "utf-8" ));

		        //HttpUriRequest request = new HttpGet(uri);

		        //request.setHeader("Accept", "application/json, text/javascript, /*; q=0.01");

		        
		        httppost.setEntity(new UrlEncodedFormEntity(pairsRestaurantDetails));

		        // Execute HTTP Post Request
		        response = httpclient.execute(httppost);
		        
		        HttpEntity httpEntity = response.getEntity();
	            InputStream is = httpEntity.getContent();
	            JSONObject json = null;
	            String output = "";
	            try {
	                BufferedReader in = new BufferedReader(new InputStreamReader(
	                        is, "iso-8859-1"), 8);
	                StringBuilder sb = new StringBuilder();
	                String line = null;
	                while ((line = in.readLine()) != null) {
	                    sb.append(line + "\n");
	                }
	                
	                sb.append(in.read());
	                is.close();
	                output = sb.toString();
	                Log.e("JSON", output);
	                json = new JSONObject(output);
	                if(thisRestaurant.getVersion() != json.getInt("VERSION")){
	                	String htmlCode = json.getString("PRESENTATION_HTML");
	                	publishProgress(htmlCode);
	                }
	            }
	            catch (Exception e) {
	                Log.e("Buffer Error", "Error converting result " + e.toString());
	            }
		    }
		    catch (Exception e) {
                Log.e("Buffer Error", e.toString());
            }
			return null;
		}
	}
}
