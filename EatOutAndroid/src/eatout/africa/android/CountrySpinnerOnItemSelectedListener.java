package eatout.africa.android;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

public class CountrySpinnerOnItemSelectedListener implements OnItemSelectedListener  {
	private final String COUNTRY_POSITION = "country position";
	private final String COUNTRY_PREFERENCES = "country preferences";
	
	private Context context;
    private SharedPreferences pref;
    private String[] countryAbb;
    private String[] countryNames;
    private Button countryButton;
    private int currentCountryIndex;
    private Spinner citySpinner;
    private ArrayList<Integer> currentCityId;
    private boolean oncreate;

    public CountrySpinnerOnItemSelectedListener(Context con, String[] country_abbrev, String[] country_names, Button countryBut, Spinner citySpin) {
        context = con;
    	pref = context.getSharedPreferences(COUNTRY_PREFERENCES, 0);
    	oncreate = true;
        countryAbb = country_abbrev;
        countryNames = country_names;
        countryButton = countryBut;
        currentCountryIndex = 0;
        for(int i = 0; i < countryAbb.length; i++){
        	if(countryAbb[i].equals(countryButton.getText())){
        		currentCountryIndex = i;
        		break;
        	}
        }
        
        citySpinner = citySpin;
        setCitySpinner((String) countryNames[currentCountryIndex]);
    }

    public int getCurrentCityId(int position){
    	return currentCityId.get(position);
    }
    
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		if(!oncreate){
			if(!countryAbb[position].equals(countryAbb[currentCountryIndex]))
			{
				currentCountryIndex = position;
				setCitySpinner(countryNames[position]);
			}
	    	countryButton.setText(countryAbb[position]);
	    	pref.edit().putInt(COUNTRY_POSITION, position).commit();
		}
		oncreate = false;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void setCitySpinner(String newCountry) {
		// TODO Auto-generated method stub
        for(int i = 0; i < countryAbb.length; i++){
        	if(countryNames[i].equals(newCountry)){
        		currentCountryIndex = i;
        		break;
        	}
        }
		ArrayList<City> cities = getFromXML(newCountry);
		ArrayList<String> cityNames = new ArrayList<String>();
		ArrayList<Integer> cityId = new ArrayList<Integer>();
		for(City city : cities){
			cityNames.add(city.getCity());
			cityId.add(city.getId());
		}
		currentCityId = cityId;
		countryButton.setText(countryAbb[currentCountryIndex]);
		ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, cityNames);
		cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		citySpinner.setAdapter(cityAdapter); 
	}
    
	private ArrayList<City> getFromXML(String tag){
		XmlPullParserFactory pullParserFactory;
		ArrayList<City> cities = null;
		try {
			pullParserFactory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = pullParserFactory.newPullParser();

		    InputStream in = context.getApplicationContext().getAssets().open("city_list.xml");
	        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);

            cities = parseXML(parser, tag);

		} catch (XmlPullParserException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cities;
	}
	
	private ArrayList<City> parseXML(XmlPullParser parser, String country) throws XmlPullParserException,IOException
	{
		ArrayList<City> cities = null;
        int eventType = parser.getEventType();
        City currentCity = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String name = null;
            String text = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                	cities = new ArrayList<City>();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    parser.next();
                    text = parser.getText();
                    if (name.equals("country-name") && text.equals(country)){
                    	do {
                    		name = parser.getName();
                    		if(name != null){
	                    		switch(eventType){
	                    		case XmlPullParser.START_TAG:
				                    if (name.equals("city")){
				                        currentCity = new City();
				                    } else if (currentCity != null){
				                        if (name.equals("name")){
				                        	parser.next();
				                            currentCity.setCity(parser.getText());
				                        } else if (name.equals("id")){
				                        	parser.next();
				                        	currentCity.setId(Integer.parseInt(parser.getText()));
				                        }  
				                    }
				                    break;
	                    		case XmlPullParser.END_TAG:
	                                name = parser.getName();
	                                if (name.equalsIgnoreCase("city") && currentCity != null){                                	
	                                	cities.add(currentCity);
	                                } 
	                    		}
                    		}
                    		eventType = parser.next();
                    	} while(name == null || !name.equals("country"));
                        eventType = XmlPullParser.END_DOCUMENT;
                    }

                    break;
                    
            }
            if(eventType != XmlPullParser.END_DOCUMENT)
            	eventType = parser.next();
        }

        return cities;
	}
}