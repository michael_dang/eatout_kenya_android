package eatout.africa.android;

public class City {
	private int id;
	private String name;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
	    this.id = id;
	}
	
	public String getCity() {
	    return name;
	}
	
	public void setCity(String city) {
	    this.name = city;
	}
	
	
	  // Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
	    return name;
	}
} 