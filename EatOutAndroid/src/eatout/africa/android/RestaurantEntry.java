package eatout.africa.android;

import java.util.ArrayList;

public class RestaurantEntry {
	int _id;
	String _name;
	String _displayname;
	String _address;
	boolean _booking;
	String _country;
	ArrayList<String> _cuisines;
	String _city;
	String _area;
	int _rating;
	int _version;
	double _longitude;
	double _latitude;
	String _fburl;
	String _twtag;
	String _html;
	
	public RestaurantEntry(){
		
	}
	
	public RestaurantEntry(int id, String name, String displayname, String address, boolean booking, String country, 
			ArrayList<String> cuisines, String city, String area, int rating, int version, double longitude, double latitude, 
			String fburl, String twtag, String html) {
		this._id = id;
		this._name = name;
		this._displayname = displayname;
		this._address = address;
		this._booking = booking;
		this._country = country;
		this._cuisines = cuisines;
		this._city = city;
		this._area = area;
		this._rating = rating;
		this._version = version;
		this._longitude = longitude;
		this._latitude = latitude;
		this._fburl = fburl;
		this._twtag = twtag;
		this._html = html;
	
	}
	
	public String getAddress() {
		return this._address;
	}
	
	public boolean getBooking() {
		return this._booking;
	}
	
	public String getCountry() {
		return this._country;
	}
	
	public ArrayList<String> getCuisines(){
		return this._cuisines;
	}
	
	public String getCuisinesAsString(){
		String toReturn;
		if(_cuisines.isEmpty())
			return "";
		else {
			toReturn = _cuisines.get(0);
			for(int i = 1; i < _cuisines.size(); i++)
				toReturn += "," + _cuisines.get(i);
		}
		
		return toReturn;
	}
	
	public String getCity() {
		return this._city;
	}
	
	public String getArea() {
		return this._area;
	}
	
	public int getId() {
		return this._id;
	}
	
	public String getName() {
		return this._name;
	}
	
	public String getDisplayName() {
		return this._displayname;
	}
	
	public int getRating(){
		return this._rating;
	}
	
	public double getLongitude(){
		return this._longitude;
	}
	
	public double getLatitude(){
		return this._latitude;
	}
	
	public String getFacebookURL(){
		return this._fburl;
	}
	
	public String getTwitterTag(){
		return this._twtag;
	}
	
	public String getHTML(){
		return this._html;
	}
	
	public int getVersion(){
		return this._version;
	}
	
	public void setAddress(String address) {
		this._address = address;
	}
	
	public void setBooking(boolean booking) {
		this._booking = booking;
	}
	
	public void setCountry(String country){
		this._country = country;
	}
	
	public void setCuisines(String cuisines){
		String toAdd = "";
		ArrayList<String> newCuisines = new ArrayList<String>();
		
		if(cuisines == "")
			return;
		else{

			for(int i = 0; i < cuisines.length(); i++){
				if(cuisines.charAt(i) == ',' || i == cuisines.length()-1){
					newCuisines.add(toAdd);
					toAdd = "";
				}
				else
					toAdd += cuisines.charAt(i);
			}
		}
		this._cuisines = newCuisines;
	}
	
	public void setCuisines(ArrayList<String> cuisines){
		this._cuisines = cuisines;
	}
	
	public void setCity(String city) {
		this._city = city;
	}
	
	public void setArea(String area) {
		this._area = area;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	
	public void setName(String name) {
		this._name = name;
	}
	
	public void setDisplayName(String displayname) {
		this._displayname = displayname;
	}
	
	public void setRating(int rating){
		this._rating = rating;
	}
	
	public void setVersion(int version){
		this._version = version;
	}
	
	public void setLongitude(double longitude){
		this._longitude = longitude;
	}
	public void setLatitude(double latitude){
		this._latitude = latitude;
	}
	public void setFacebookURL(String fburl){
		this._fburl = fburl;
	}
	public void setTwitterTag(String twtag){
		this._twtag = twtag;
	}
	public void setHTML(String HTML){
		this._html = HTML;
	}

}
