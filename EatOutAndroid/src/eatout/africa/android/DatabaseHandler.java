package eatout.africa.android;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper{
	
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_PATH = "data/data/eatout.africa.android/databases";
	private static final String DATABASE_NAME = "restaurantDatabase";
	private static final String TABLE_NAME = "restaurantDetailsV2";
	private static final String HTML_TABLE_NAME = "restaurantsHTML";
	
	private static final HashMap<String, String> cuisineMap = new HashMap<String, String>();
	
	private int currentIndex;
	
	//Restaurants Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_DISPLAY_NAME = "displayname";
	private static final String KEY_ADDRESS = "address";
	private static final String KEY_ALLOW_BOOKING = "allowbooking";
	private static final String KEY_COUNTRY = "country";
	private static final String KEY_CITY = "city";
	private static final String KEY_AREA = "area";
	private static final String KEY_VERSION = "version";
	private static final String KEY_LEVEL = "level";
	private static final String KEY_LONGITUDE = "longitude";
	private static final String KEY_LATITUDE = "latitude";
	private static final String KEY_CUISINES = "cuisines";
	private static final String KEY_FACEBOOK_URL = "fburl";
	private static final String KEY_TWITTER_TAG = "twittertag";
	private static final String KEY_PRESENTATION_HTML = "html";
	
	/* column numbers in sqlite table:
	 	0.) idx
	    1.) _id
	 	2.) version
		3.) name
		4.) display_name
		5.) address
		6.) allow_booking
		7.) level
		8.) longitude
		9.) latitude
		10.) cousine_id
		11.) country
		12.) city
		13.) area
		14.) facebook_url
		15.) twitter_tag
		16.) html
		17.) phone

	 */
	
	private final Context context;
	private SQLiteDatabase db;
	
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;

		String[] cuisineIds = context.getResources().getStringArray(R.array.cuisine_ids);
		String[] cuisines = context.getResources().getStringArray(R.array.cuisine_entries);
		
		for(int i = 0; i < cuisineIds.length; i++){
			cuisineMap.put(cuisineIds[i],cuisines[i]);
		}
		
		currentIndex = 0;
	}
	
	//Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		/*
		String CREATE_RESTAURANTS_TABLE = "CREATE TABLE " + TABLE_RESTAURANTSA + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
				+ KEY_DISPLAY_NAME + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_ALLOW_BOOKING + " TEXT,"
				+ KEY_COUNTRY + " TEXT," + KEY_CITY + " TEXT," + KEY_AREA + " TEXT," + KEY_VERSION + " TEXT," + 
				KEY_LEVEL + " TEXT," + KEY_LONGITUDE + " TEXT," + KEY_LATITUDE + " TEXT," +  KEY_CUISINES + " TEXT," + 
				KEY_FACEBOOK_URL + " TEXT," + KEY_TWITTER_TAG + " TEXT," + KEY_PRESENTATION_HTML + " TEXT" + ")";
		db.execSQL(CREATE_RESTAURANTS_TABLE);
		*/
	}
	
	public void makeDatabase() throws IOException{
		
		boolean dbExist = checkDataBase();
		 
    	if(dbExist){
    		//do nothing - database already exist
    	}else{
 
    		//By calling this method and empty database will be created into the default system path
               //of your application so we are gonna be able to overwrite that database with our database.
        	this.getReadableDatabase();
 
        	try {
 
    			copyDataBase();
 
    		} catch (IOException e) {
 
        		throw new Error("Error copying database");
 
        	}
    	}
	}
	
    private void copyDataBase() throws IOException{
    	 
    	//Open your local db as the input stream
    	InputStream myInput = context.getAssets().open(DATABASE_NAME);
 
    	// Path to the just created empty db
    	String outFileName = DATABASE_PATH + DATABASE_NAME;
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
	
    private boolean checkDataBase(){
    	 
    	SQLiteDatabase checkDB = null;
 
    	try{
    		String myPath = DATABASE_PATH + DATABASE_NAME;
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
    	}catch(SQLiteException e){
 
    		//database does't exist yet.
 
    	}
 
    	if(checkDB != null){
 
    		checkDB.close();
 
    	}
 
    	return checkDB != null ? true : false;
    }
    
    public void openDataBase() throws SQLException{
    	 
    	//Open the database
        String myPath = DATABASE_PATH + DATABASE_NAME;
    	db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
    }
 
    @Override
	public synchronized void close() {
 
    	    if(db != null)
    		    db.close();
 
    	    super.close();
 
	}

        // Add your public helper methods to access and get content from the database.
       // You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
       // to you to create adapters for your views.
 
    
	
	//Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		
		//Create tables again
		onCreate(db);
	}
	
	public void addRestaurant(RestaurantEntry restaurant) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_ID, restaurant.getId());
		values.put(KEY_NAME, restaurant.getName());
		values.put(KEY_DISPLAY_NAME, restaurant.getDisplayName());
		values.put(KEY_ADDRESS, restaurant.getAddress());
		values.put(KEY_ALLOW_BOOKING, restaurant.getBooking());
		values.put(KEY_COUNTRY, restaurant.getCountry());
		values.put(KEY_CITY, restaurant.getCity());
		values.put(KEY_AREA, restaurant.getArea());
		values.put(KEY_VERSION, restaurant.getVersion());
		values.put(KEY_LEVEL, restaurant.getRating());
		values.put(KEY_LONGITUDE, restaurant.getLongitude());
		values.put(KEY_LATITUDE, restaurant.getLatitude());
		values.put(KEY_CUISINES, restaurant.getCuisinesAsString());
		values.put(KEY_FACEBOOK_URL, restaurant.getFacebookURL());
		values.put(KEY_TWITTER_TAG, restaurant.getTwitterTag());
		values.put(KEY_PRESENTATION_HTML, restaurant.getHTML());
		
		
		
		db.insert(TABLE_NAME, null, values);
		db.close();
	}
	
	public RestaurantEntry getRestaurant(int id){
		String htmlSearchQuery = "SELECT * FROM " + HTML_TABLE_NAME + " WHERE _id = '" + id + "'";
		String searchQuery = "SELECT * FROM " + TABLE_NAME + " WHERE _id = '" + id + "'";
		Cursor cursor = db.rawQuery(htmlSearchQuery, null);
		String html = "";
		if(cursor.moveToFirst())
			html = cursor.getString(1);
		
		cursor = db.rawQuery(searchQuery, null);
		if(cursor.moveToFirst()){
			RestaurantEntry restaurant = new RestaurantEntry();
			restaurant.setId(Integer.parseInt(cursor.getString(1)));
			restaurant.setName(cursor.getString(3));
			restaurant.setDisplayName(cursor.getString(4));
			restaurant.setAddress(cursor.getString(5));
			restaurant.setBooking(cursor.getString(6) == "TRUE");
			restaurant.setCountry(cursor.getString(11));
			restaurant.setCity(cursor.getString(12));
			restaurant.setArea(cursor.getString(13));
			restaurant.setCuisines(cursor.getString(10));//Id's or names?
			restaurant.setVersion(Integer.parseInt(cursor.getString(2)));
			restaurant.setRating(Integer.parseInt(cursor.getString(7)));
			restaurant.setLongitude(Double.parseDouble(cursor.getString(8)));
			restaurant.setLatitude(Double.parseDouble(cursor.getString(9)));
			restaurant.setFacebookURL(cursor.getString(14));
			restaurant.setTwitterTag(cursor.getString(15));
			restaurant.setHTML(html);
			//restaurant.setPhone(cursor.getString(17);
			return restaurant;
		}
		return null; //should never occur
			
	}
	
/*	public RestaurantBasic searchResults(ArrayList<String> searchTerms, booleanFromBeginning){
		
		
		
		
	}*/
	
	
	public RestaurantBasic exploreResults(int numberToSearch, String city, String area, String cuisineId, int level, boolean searchFromBeginning){
		
		if(searchFromBeginning)
			currentIndex = 0;
		
		String searchQuery = "SELECT * FROM " + TABLE_NAME + " WHERE city = '" +  city + "'" + " AND idx >= '" + currentIndex + "'" + " AND level = '" + level + "'";
		int currentNumber = 0;
		if(!area.equals("Area..."))
			searchQuery += " AND area = '" + area + "'";
		
		Cursor cursor = db.rawQuery(searchQuery, null);
		
		if(cursor.moveToFirst()){
			do{
				ArrayList<String> cuisinesToAdd = new ArrayList<String>();
				currentIndex = cursor.getInt(0);
				int restaurantId = cursor.getInt(1);
				String name = cursor.getString(4);
				String address = cursor.getString(5);
				int rating = cursor.getInt(7);
				String currentCuisineId = cursor.getString(10);
				
				String checkCuisine = "";
				char currentChar;
				boolean addRestaurant = false;
				
				/*if(cuisineId.equals("-1")){
					currentNumber++;
					RestaurantBasic toAdd = new RestaurantBasic(name, restaurantId, cuisinesToAdd, address, rating);
					basicRestaurantList.add(toAdd);
					Log.d("found restaurant", name);
				}*/
				
				for(int i = 0; i < currentCuisineId.length(); i++){
					currentChar = currentCuisineId.charAt(i);
					if(currentChar == ',' || i == currentCuisineId.length()-1){
						if(i == currentCuisineId.length()-1)
							checkCuisine += currentChar;
						cuisinesToAdd.add(cuisineMap.get(checkCuisine));
						
						if(checkCuisine.equals(cuisineId)){
							addRestaurant = true;
						}
						
						checkCuisine = "";
					}
					else
						checkCuisine += currentChar;
				}
				if(addRestaurant || cuisineId.equals("-1")){
					currentNumber++;
					Log.d("found restaurant", name);
					currentIndex++;
					return new RestaurantBasic(name, restaurantId, cuisinesToAdd, address, rating, currentIndex-1);
				}
				

			}while(cursor.moveToNext() && currentNumber < numberToSearch);
		}
		
		currentIndex++;
		return null;
	}
	
	
	public ArrayList<RestaurantEntry> getAllRestaurants() {
		ArrayList<RestaurantEntry> restaurantList = new ArrayList<RestaurantEntry>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME;
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		if (cursor.moveToFirst()) {
			do {
				RestaurantEntry restaurant = new RestaurantEntry();
				restaurant.setId(Integer.parseInt(cursor.getString(0)));
				restaurant.setName(cursor.getString(1));
				restaurant.setDisplayName(cursor.getString(2));
				restaurant.setAddress(cursor.getString(3));
				restaurant.setBooking(cursor.getString(4) == "TRUE");
				restaurant.setCountry(cursor.getString(5));
				restaurant.setCity(cursor.getString(6));
				restaurant.setArea(cursor.getString(7));
				restaurant.setCuisines(cursor.getString(12));
				restaurant.setVersion(Integer.parseInt(cursor.getString(8)));
				restaurant.setRating(Integer.parseInt(cursor.getString(9)));
				restaurant.setLongitude(Double.parseDouble(cursor.getString(10)));
				restaurant.setLatitude(Double.parseDouble(cursor.getString(11)));
				restaurant.setFacebookURL(cursor.getString(13));
				restaurant.setTwitterTag(cursor.getString(14));
				restaurant.setHTML(cursor.getString(15));
				
				
				restaurantList.add(restaurant);
				
			} while (cursor.moveToNext());
		}
		return restaurantList;
		
	}
	
	//Getting restaurants count
	public int getRestaurantsCount() {
		String countQuery = "SELECT * FROM " + TABLE_NAME;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		
		//return count
		return cursor.getCount();
	}
}
