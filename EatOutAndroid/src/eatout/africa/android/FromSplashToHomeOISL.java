package eatout.africa.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

public class FromSplashToHomeOISL implements OnItemSelectedListener  {
	private final String COUNTRY_POSITION = "country position";
	private final String COUNTRY_PREFERENCES = "country preferences";
	
	private Context context;
    private SharedPreferences pref;
    private String[] countryNames;
    private boolean oncreate;

    public FromSplashToHomeOISL(Context con, String[] country_names) {
        context = con;
        oncreate = true;
    	pref = context.getSharedPreferences(COUNTRY_PREFERENCES, 0);
        countryNames = country_names;
    }
    
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position, //isn't being called when item is clicked...
			long arg3) {
		if(!oncreate){
			Intent toHomePage = new Intent(context, HomePage.class);
			toHomePage.putExtra("new country", countryNames[position]);
			toHomePage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    	pref.edit().putInt(COUNTRY_POSITION, position).commit();
			context.startActivity(toHomePage);
		}
		oncreate = false;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}