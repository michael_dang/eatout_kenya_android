package eatout.africa.android;

import java.util.ArrayList;

public class RestaurantBasic {

	private int index;
	private String name;
	private int id;
	private ArrayList<String> cuisines;
	//private int[] cuisineIds;
	private String address;
	private int rating;
	
	public RestaurantBasic(String name, int id, ArrayList<String> cuisines, String address, int rating, int index){//int[] cuisineIds, String address, int rating) {
		// TODO Auto-generated constructor stub
		this.index = index;
		this.name = name;
		this.id = id;
		this.cuisines = cuisines;
		//this.cuisineIds = cuisineIds;
		this.address = address;
		this.rating = rating;
	}
	
	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}
	
	public ArrayList<String> getCuisines(){
		return cuisines;
	}
	
	public String getCuisinesAsString(){
		if(cuisines.isEmpty())
			return "";
		String toReturn = cuisines.get(0);
		for(int i = 1; i < cuisines.size(); i++){
			toReturn += ", " + cuisines.get(i);
		}
		return toReturn;
	}
	
	public String getAddress(){
		return address;
	}
	
	public int getRating(){
		return rating;
	}
	
	public int getIndex(){
		return index;
	}
	
}
