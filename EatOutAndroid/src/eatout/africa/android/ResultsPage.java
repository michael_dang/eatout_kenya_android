package eatout.africa.android;

import java.io.BufferedReader;
import java.io.IOException;
/*
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;*/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/*
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
*/
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ResultsPage extends Activity {

	private final String CUISINE_NONE_SELECTED = "Cuisine...";
	private final String CUISINE_ID_NONE_SELECTED = "-1";
	private final String AREA_NONE_SELECTED = "Area...";
	private final String COUNTRY_PREFERENCES = "country preferences";
	private final String COUNTRY_POSITION = "country position";
	private final String NEW_COUNTRY = "new country";
	private final String FINISHED_SEARCHING = "Finished searching all restaurants";
	private final int KENYA_DEFAULT_COUNTRY = 3;
	private final int NUMBER_OF_RESTAURANTS_PER_PAGE = 9;

	private ArrayList<RestaurantBasic> restaurants = null;
	private SharedPreferences countryPrefs;
	private Spinner countrySpinner;
	private ProgressBar getRestaurantsProgressBar;
	private ImageButton homeButton;
	private Button countryButton;
	private Button filterButton;
	private Button sortButton;
	private ImageButton backButton;
	private TextView searchTerms;
	private ListView resultsList;
	private String[] info;
	
	//Search Function variables
	private boolean searchFinished = false;
	private boolean searchFromBeginning = true;
	private int currentRating = 3;
	private int numberCurrentRestaurants = 0;
	private int restaurantIndex = 0;
	private DatabaseHandler db;
	RestaurantListItemAdapter listAdapter;
	private SearchResults results;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results_page);
		countrySpinner = (Spinner) findViewById(R.id.country_spinner);
		homeButton = (ImageButton) findViewById(R.id.home_button);
		countryButton = (Button) findViewById(R.id.country_button);
		sortButton = (Button) findViewById(R.id.sort_button);
		filterButton = (Button) findViewById(R.id.filter_button);
		backButton = (ImageButton) findViewById(R.id.back_button);
		resultsList = (ListView) findViewById(R.id.restaurant_list);
		getRestaurantsProgressBar = (ProgressBar) findViewById(R.id.get_restaurants_progress_bar);

		Intent infoIntent = getIntent();
		info = infoIntent.getStringArrayExtra("info");
		int cityId = infoIntent.getIntExtra("cityId", 1); // ADJUST BECAUSE
															// INTENT NOW HAS
															// DIFFERENT STUFF

		countryPrefs = getSharedPreferences(COUNTRY_PREFERENCES, 0);
		searchTerms = (TextView) findViewById(R.id.search_terms);
		if (info.length > 1) {
			String cuisineDisplay = info[3];
			String areaDisplay = info[1];
			if (cuisineDisplay.equals(CUISINE_NONE_SELECTED))
				cuisineDisplay = "Any";
			if (areaDisplay.equals(AREA_NONE_SELECTED))
				areaDisplay = "any place";
			searchTerms.setText(cuisineDisplay + " food in " + areaDisplay);
		} else
			searchTerms.setText(info[0]);

		db = new DatabaseHandler(this);
		
		// begin searching
		results = new SearchResults();
		results.execute();

		// need to access database of restaurant info. AKA API calls to
		// getRestaurantDetails
		// then you'll have an array of RestaurantListItem hopefully
		resultsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				Intent resInfoPage = new Intent(ResultsPage.this, RestaurantInfoPage.class);
				RestaurantBasic picked = (RestaurantBasic) resultsList.getItemAtPosition(pos);
				resInfoPage.putExtra("restaurant Id", picked.getId());
				startActivity(resInfoPage);

			}

		});
		resultsList.setOnScrollListener(new OnScrollListener() {

			int currentVisibleItemCount = 0;
			int lastItem = 0;
			int totalCount = 0;

			@Override
			public void onScroll(AbsListView lw, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				
				lastItem = firstVisibleItem + visibleItemCount;
				totalCount = totalItemCount;
				currentVisibleItemCount = visibleItemCount;
			}

			@Override
			public void onScrollStateChanged(AbsListView arg0, int scrollState) {
				// TODO Auto-generated method stub
				
				if(this.totalCount == lastItem && scrollState == SCROLL_STATE_IDLE){
					View view = (View) resultsList.getChildAt(resultsList.getChildCount()-1);
			        int diff = (view.getBottom()-(resultsList.getHeight()+resultsList.getScrollY()));// Calculate the scrolldiff
			        if( diff == 0 ){  // if diff is zero, then the bottom has been reached
			        	
						searchFinished = false;
						new SearchResults().execute();
			        }
		        }
			}

		});

		int countryPos = countryPrefs.getInt(COUNTRY_POSITION,
				KENYA_DEFAULT_COUNTRY);
		final String[] countryAbbrev = getResources().getStringArray(
				R.array.country_abbrev);
		final String[] countryNames = getResources().getStringArray(
				R.array.country_entries);

		countrySpinner.setSelection(countryPos);
		countryButton.setText(countryAbbrev[countryPos]);
		countrySpinner
				.setOnItemSelectedListener(new ToHomePageCountrySpinnerOISL(
						getBaseContext(), countryAbbrev, countryNames,
						countryButton));

		countryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				new AlertDialog.Builder(ResultsPage.this)
						.setMessage("Do you want to switch countries?")
						.setPositiveButton(android.R.string.yes,
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int whichButton) {
										countrySpinner.performClick();
									}
								}).setNegativeButton(android.R.string.no, null)
						.show();
			}
		});

        homeButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {

				// TODO Auto-generated method stub
		    	new AlertDialog.Builder(ResultsPage.this)
		    	.setMessage("Go back to the home page?")
		    	.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    	    public void onClick(DialogInterface dialog, int whichButton) {
						Intent toHomePage = new Intent(ResultsPage.this, HomePage.class);
						startActivity(toHomePage);
		    	    }})
		    	 .setNegativeButton(android.R.string.no, null).show();
			}
        	
        });
		
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}

		});
		
		sortButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
								
				new AlertDialog.Builder(ResultsPage.this).setTitle("Sort by:")
					.setItems(R.array.sort_options, new DialogInterface.OnClickListener()	
				{
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Comparator<RestaurantBasic> NameComparatorA = new Comparator<RestaurantBasic>(){
							@Override
							public int compare(RestaurantBasic A,
									RestaurantBasic B) {
								return A.getName().compareTo(B.getName());
							}
						};
						
						Comparator<RestaurantBasic> NameComparatorZ = new Comparator<RestaurantBasic>(){
							@Override
							public int compare(RestaurantBasic A,
									RestaurantBasic B) {
								return B.getName().compareTo(A.getName());
							}
						};
						
						Comparator<RestaurantBasic> RatingComparatorHigh = new Comparator<RestaurantBasic>(){
							@Override
							public int compare(RestaurantBasic A,
									RestaurantBasic B) {
								int difference = B.getRating() - A.getRating();
								if(difference == 0)
									return A.getName().compareTo(B.getName());
								return difference;
							}
						};
						
						Comparator<RestaurantBasic> RatingComparatorLow = new Comparator<RestaurantBasic>(){
							@Override
							public int compare(RestaurantBasic A,
									RestaurantBasic B) {
								int difference = A.getRating() - B.getRating();
								if(difference == 0)
									return A.getName().compareTo(B.getName());
								return difference;
							}
						};
						switch(which){
						case 0:
							Collections.sort(restaurants, RatingComparatorHigh);
							break;
						case 1:
							Collections.sort(restaurants, RatingComparatorLow);
							break;
						case 2:
							Collections.sort(restaurants, NameComparatorA);
							break;
						case 3:
							Collections.sort(restaurants, NameComparatorZ);
							
						}
						
						listAdapter = new RestaurantListItemAdapter(getBaseContext(),
								restaurants);
						resultsList.setAdapter(listAdapter);
						listAdapter.notifyDataSetChanged();
						
					}
				}).show();
				

			}
			
		});

		filterButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

			}

		});
	}
	

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		// SearchResults continueSearching = new SearchResults();
		// continueSearching.execute();
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		// results.cancel(false);
		super.onPause();
	}

	private class SearchResults extends AsyncTask<String, RestaurantBasic, Void> {

		RestaurantBasic restaurantToAdd;
		JSONArray allJSONRestaurants = null;


		@Override
		protected Void doInBackground(String... searchTerms) {
			// TODO Auto-generated method stub
			
			try {
				db.makeDatabase();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			db.openDataBase();
			String area = null;
			String cuisineId = null;

			Intent infoIntent = getIntent();
			String info[] = infoIntent.getStringArrayExtra("info");
			String cityName = info[0];			
			
			
			boolean isExplore = infoIntent.getBooleanExtra("isExplore", true);

			if (isExplore) {
				area = info[1];
				cuisineId = info[2];
			}

			//restaurantsToDisplay = db.exploreResults(NUMBER_OF_RESTAURANTS_PER_PAGE, cityName, area, cuisineId);
			for(int i = 0; i < NUMBER_OF_RESTAURANTS_PER_PAGE; i++){
				restaurantToAdd = db.exploreResults(1, cityName, area, cuisineId, currentRating, searchFromBeginning);
				searchFromBeginning = false;
				//publishProgress(restaurantToAdd);
				if(restaurantToAdd == null){
					if(currentRating == 0){
						publishProgress(restaurantToAdd);
						searchFinished = true;
						break;
					}
					else{
						currentRating --;
						searchFromBeginning = true;
						continue;
					}
				}
				if(restaurantToAdd == null && currentRating == 0){
					publishProgress(restaurantToAdd);
					break;
				}
				else
					publishProgress(restaurantToAdd);
			}
			//publishProgress(restaurantsToDisplay.get(i));
			//OR WE CAN DO
			//restaurantToAdd = db.exploreResults(1, cityName, area, cuisineId);
			
			
			/*
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://www.eatout.co.ke/api/20131024/restaurant/getRestaurantsByCity.php");
			HttpResponse response = null;

			// for(int k = 1; k < 39; k++){ //one of the restaurants crash the
			// app.
			// if(k == 2)
			// k++;
			try {

				List<NameValuePair> pairsRestaurants = new ArrayList<NameValuePair>(
						3);
				pairsRestaurants
						.add(new BasicNameValuePair("login", "android"));
				pairsRestaurants.add(new BasicNameValuePair("password",
						"dangmichael"));
				pairsRestaurants.add(new BasicNameValuePair("cityId", String
						.valueOf(cityId)));

				httppost.setEntity(new UrlEncodedFormEntity(pairsRestaurants));

				// Execute HTTP Post Request
				response = httpclient.execute(httppost);

				HttpEntity httpEntity = response.getEntity();
				InputStream is = httpEntity.getContent();
				JSONObject json = null;
				String output = "";
				try {

					BufferedReader in = new BufferedReader(
							new InputStreamReader(is, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					if (allJSONRestaurants == null) {
						while ((line = in.readLine()) != null) {
							sb.append(line + "\n");
						}

						sb.append(in.read());
						is.close();
						output = sb.toString();
						Log.e("JSON", output);
						json = new JSONObject(output);
						allJSONRestaurants = json.getJSONArray("RESULT");
					}

					if (isExplore) {
						List<NameValuePair> pairsRestaurantDetails = new ArrayList<NameValuePair>(
								4);
						pairsRestaurantDetails.add(new BasicNameValuePair(
								"login", "android"));
						pairsRestaurantDetails.add(new BasicNameValuePair(
								"password", "dangmichael"));
						pairsRestaurantDetails.add(new BasicNameValuePair(
								"restaurantId", String.valueOf(cityId)));
						pairsRestaurantDetails.add(new BasicNameValuePair(
								"version", ""));

						JSONObject JSONRestaurantDetails;
						Log.e("resturants",
								String.valueOf(allJSONRestaurants.length()));
						httppost = new HttpPost(
								"http://www.eatout.co.ke/api/20131024/restaurant/getRestaurantDetails.php");

						for (int i = numberCurrentRestaurants; i < numberCurrentRestaurants
								+ NUMBER_OF_RESTAURANTS_PER_PAGE
								&& i < allJSONRestaurants.length();) {
							int restaurantId = (Integer) allJSONRestaurants
									.getJSONObject(restaurantIndex).get("ID");
							restaurantIndex++;
							Log.e("Restaurant ID", String.valueOf(i));
							pairsRestaurantDetails.set(3,
									new BasicNameValuePair("restaurantId",
											String.valueOf(restaurantId)));
							httppost.setEntity(new UrlEncodedFormEntity(
									pairsRestaurantDetails));
							// Execute HTTP Post Request
							response = httpclient.execute(httppost);

							httpEntity = response.getEntity();

							is = httpEntity.getContent();

							in = new BufferedReader(new InputStreamReader(is,
									"iso-8859-1"), 8);
							sb = new StringBuilder();
							line = null;

							while ((line = in.readLine()) != null) {
								sb.append(line + "\n");
							}
							sb.append(in.read());
							is.close();
							output = sb.toString();
							if (output.equals("-1")) {

								continue;
							}
							Log.e("JSON Restaurant Details", output);
							JSONRestaurantDetails = new JSONObject(output);

							JSONArray JSONCuisines = JSONRestaurantDetails
									.getJSONArray("COUSINE");

							String[] cuisines = new String[JSONCuisines
									.length()];
							String[] cuisineEntries = getBaseContext()
									.getResources().getStringArray(
											R.array.cuisine_entries);
							String[] cuisineIds = getBaseContext()
									.getResources().getStringArray(
											R.array.cuisine_ids);

							boolean containsCuisine = false;
							if (cuisineId.equals(CUISINE_ID_NONE_SELECTED))
								containsCuisine = true;

							ArrayList<String> cuisineArrayIds = new ArrayList<String>(
									Arrays.asList(cuisineIds));
							for (int j = 0; j < JSONCuisines.length(); j++) {
								int id = JSONCuisines.getJSONObject(j).getInt(
										"ID");
								int idx = cuisineArrayIds.indexOf(String
										.valueOf(id));
								Log.d("cuisine idx", String.valueOf(idx));

								if (idx >= 0) {
									cuisines[j] = cuisineEntries[idx];
									if (cuisineId == String.valueOf(id))
										containsCuisine = true;
									Log.d("cuisine", cuisines[j]);
								}
							}
							if ((JSONRestaurantDetails.get("AREA").equals(area) || area
									.equals(AREA_NONE_SELECTED))
									&& containsCuisine) {
								i++;
								String name = (String) JSONRestaurantDetails
										.get("NAME");
								name = name.trim();
								String displayName = (String) JSONRestaurantDetails
										.get("DISPLAY_NAME");
								String address = (String) JSONRestaurantDetails
										.get("ADDRESS");
								boolean booking = (boolean) JSONRestaurantDetails
										.getBoolean("ALLOW_BOOKING");
								String country = (String) JSONRestaurantDetails
										.get("COUNTRY");
								String city = (String) JSONRestaurantDetails
										.get("CITY");
								int rating = (Integer) JSONRestaurantDetails
										.getInt("LEVEL");
								int version = JSONRestaurantDetails
										.getInt("VERSION");
								double longitude = (Double) JSONRestaurantDetails
										.getDouble("LONGITUDE");
								double latitude = (Double) JSONRestaurantDetails
										.getDouble("LONGITUDE");
								String fburl = (String) JSONRestaurantDetails
										.get("FACEBOOK_URL");
								String twtag = (String) JSONRestaurantDetails
										.get("TWITTER_TAG");
								String html = (String) JSONRestaurantDetails
										.get("PRESENTATION_HTML");

								restaurantToAdd = new RestaurantEntry(
										restaurantId, name, displayName,
										address, booking, country, cuisines,
										city, area, rating, version, longitude,
										latitude, fburl, twtag, html);
								// db.addRestaurant(restaurantToAdd);

								publishProgress(restaurantToAdd);
							}
						}

					}

				} catch (Exception e) {
					Log.e("Buffer Error",
							"Error converting result " + e.toString());
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			// response.getEntity().*/

			return null;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if(restaurants == null){
				restaurants = new ArrayList<RestaurantBasic>();
	
				listAdapter = new RestaurantListItemAdapter(getBaseContext(),
						restaurants);
				resultsList.setAdapter(listAdapter);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			numberCurrentRestaurants += NUMBER_OF_RESTAURANTS_PER_PAGE;
			Log.d("asynctask", "finished running");
			db.close();
			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled(Void result) {
			// TODO Auto-generated method stub
			super.onCancelled(result);
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onProgressUpdate(RestaurantBasic... values) {
			// TODO Auto-generated method stub
			if (getRestaurantsProgressBar.getVisibility() == View.VISIBLE)
				getRestaurantsProgressBar.setVisibility(View.GONE);
			if(values[0] != null)
				listAdapter.add(values[0]);
			else
				Toast.makeText(getBaseContext(),FINISHED_SEARCHING, 
		                Toast.LENGTH_SHORT).show();
			super.onProgressUpdate(values);
		}

	}

}
