package eatout.africa.android;

import java.util.ArrayList;

import eatout.africa.android.RestaurantListItem;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RestaurantListItemAdapter extends ArrayAdapter<RestaurantBasic>{
	
	private Context context;
	private ArrayList<RestaurantBasic> restaurants;
	private static LayoutInflater inflater = null;

	public RestaurantListItemAdapter(Context context, ArrayList<RestaurantBasic> restaurants){
		super(context, R.layout.list_item, restaurants);
		this.context = context;
		this.restaurants = restaurants;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return restaurants.size();
	}


	@Override
	public RestaurantBasic getItem(int pos) {
		// TODO Auto-generated method stub
		return restaurants.get(pos);
	}


	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return restaurants.get(pos).getId();
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_item, null);
 
        TextView restaurantName = (TextView)vi.findViewById(R.id.restaurant_name); // name
        TextView restaurantAddress = (TextView)vi.findViewById(R.id.restaurant_address); // address
        TextView restaurantCuisine = (TextView)vi.findViewById(R.id.restaurant_cuisine); // cuisine
        ImageView ratingImage = (ImageView)vi.findViewById(R.id.restaurant_rating); // rating image

        RestaurantBasic restaurant = restaurants.get(position);
        // Setting all values in listview
        restaurantName.setText(restaurant.getName());
        restaurantAddress.setText(restaurant.getAddress());
        ArrayList<String> cuisines = restaurant.getCuisines();
        String cuisinesText = "";
        if(!cuisines.isEmpty())
        	cuisinesText = cuisines.get(0);
        for (int i = 1; i < cuisines.size(); i++)
        {
        	cuisinesText += ", " + cuisines.get(i);
        }
        	
        restaurantCuisine.setText(cuisinesText);
        
        /*public static int getResourceId(Context context, String name, String resourceType) {
            return context.getResources().getIdentifier(name, resourceType, context.getPackageName());
        }

        int iconId = getResourceId(Activity.this, image, "drawable");    

        ImageView categoryIcon = (ImageView) v.findViewById(R.id.category_icon);
        categoryIcon.setImageResource(iconId);*/
        
        Drawable ratingDrawable = null;
        switch (restaurant.getRating()){
        	case RestaurantListItem.BRONZE_RATING:
        		ratingDrawable = context.getResources().getDrawable(R.drawable.bronze);
        		break;
        	case RestaurantListItem.SILVER_RATING:
        		ratingDrawable = context.getResources().getDrawable(R.drawable.silver);
        		break;
        	case RestaurantListItem.GOLD_RATING:
        		ratingDrawable = context.getResources().getDrawable(R.drawable.gold);
        		break;
        }
        ratingImage.setImageDrawable(ratingDrawable);
        return vi;
    }
	

}