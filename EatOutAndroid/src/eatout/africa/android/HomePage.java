package eatout.africa.android;

/* keystore password: eatoutafrica
 * alias: EatoutVersion1.0 password: eatout
 */

import java.util.Arrays;

import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ActionBar.LayoutParams;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class HomePage extends FragmentActivity {


	private PagerAdapter topPicksPagerAdapter;
	private TopPicksViewPager topPicksPager;
	private final int NUM_PAGES = 3;
	private final int KENYA_DEFAULT_COUNTRY = 3;
	private final String COUNTRY_PREFERENCES = "country preferences";
	private final String COUNTRY_POSITION = "country position";
	private final String NEW_COUNTRY = "new country";
    private SharedPreferences countryPrefs;
	
    private CountrySpinnerOnItemSelectedListener countryOISL;
	private Spinner countrySpinner;
	private Spinner cuisineSpinner;
	private Spinner citySpinner;
	private Spinner areaSpinner;
	private LinearLayout exploreSearchBody;
	private RelativeLayout cityLayout;
	private RelativeLayout cuisineLayout;
	private RelativeLayout areaLayout;
	private RelativeLayout goButtonLayout;
	private EditText searchBar;
	private ImageButton homeButton;
	private Button countryButton;
	private Button goButton;
	private Button searchButton;
	private Button exploreButton;
	private Boolean exploreHasFocus;
	private boolean setSearchDefault = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        
 	   
        //runFirstTime();
	   
	   
 	   //Prints out all the entries in the database
       /*
 	   DatabaseHandler db = new DatabaseHandler(this);
 	   Log.d("Reading: ", "Reading all restaurants..");
 	   List<RestaurantEntry> restaurants = db.getAllRestaurants();
 	   
 	   for (RestaurantEntry cn : restaurants) {
 		   String log = "ID: " + cn.getId()+", Name: " + cn.getName() + ", Display Name: " + cn.getDisplayName() + ", Address: " + cn.getAddress() + " ,Allow Booking: " + cn.getBooking() +
 				   " ,Country: " + cn.getCountry() + " ,City: " + cn.getCity() + " ,Area: " + cn.getArea() + "Longitude " + cn.getLongitude();
 		   Log.d("Name: ",log);}
        db.close();
        */
        exploreHasFocus = true;        
        countryPrefs = getSharedPreferences(COUNTRY_PREFERENCES, 0);
        
        cityLayout = (RelativeLayout) findViewById(R.id.city_layout);
        areaLayout = (RelativeLayout) findViewById(R.id.area_layout);
        cuisineLayout = (RelativeLayout) findViewById(R.id.cuisine_layout);
        cuisineSpinner = (Spinner) findViewById(R.id.cuisine_spinner);
        citySpinner = (Spinner) findViewById(R.id.city_spinner);
        areaSpinner = (Spinner) findViewById(R.id.area_spinner);
        searchBar = (EditText) findViewById(R.id.keyword_search);
        goButtonLayout = (RelativeLayout) findViewById(R.id.go_button_layout);
        exploreSearchBody = (LinearLayout) findViewById(R.id.explore_search);
        
        countryButton = (Button) findViewById(R.id.country_button);
        goButton = (Button) findViewById(R.id.go_button);
        searchButton = (Button) findViewById(R.id.search_button);
        exploreButton = (Button) findViewById(R.id.explore_button);
        citySpinner.setSelection(countryPrefs.getInt("current city", 0));
        citySpinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				countryPrefs.edit().putInt("current city", arg2).commit();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        
        searchButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if(exploreHasFocus){
					searchButton.setBackgroundColor(getResources().getColor(R.color.transparent_eatout_orange));
					exploreButton.setBackgroundColor(getResources().getColor(R.color.transparent_black));
					
					int height_go = goButtonLayout.getMeasuredHeight();
					int height_cuisine = cuisineLayout.getMeasuredHeight();
					int height_city = areaLayout.getMeasuredHeight();
										
					areaLayout.setVisibility(View.GONE);
					cuisineSpinner.setVisibility(View.GONE);
					searchBar.setVisibility(View.VISIBLE);
					
					LinearLayout.LayoutParams shrink_cuisine = new LinearLayout.LayoutParams(
	                        LayoutParams.MATCH_PARENT,
	                        height_cuisine);
					
					LinearLayout.LayoutParams shrink_city = new LinearLayout.LayoutParams(
	                        LayoutParams.MATCH_PARENT,
	                        height_city);
					
					LinearLayout.LayoutParams shrink_go = new LinearLayout.LayoutParams(
	                        LayoutParams.MATCH_PARENT,
	                        height_go);

					LinearLayout.LayoutParams shrink_body = new LinearLayout.LayoutParams(
	                        LayoutParams.MATCH_PARENT,
	                        LayoutParams.WRAP_CONTENT);
					
					exploreSearchBody.setLayoutParams(shrink_body);
					

					goButtonLayout.setLayoutParams(shrink_go);
					cuisineLayout.setLayoutParams(shrink_cuisine);
					cityLayout.setLayoutParams(shrink_city);
					
					
					exploreHasFocus = false;
				}
			}
        });
        exploreButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				if(!exploreHasFocus){
					exploreButton.setBackgroundColor(getResources().getColor(R.color.transparent_eatout_orange));
					searchButton.setBackgroundColor(getResources().getColor(R.color.transparent_black));

					searchBar.setVisibility(View.GONE);
					cuisineSpinner.setVisibility(View.VISIBLE);
					
					
					areaLayout.setVisibility(View.VISIBLE);
					LinearLayout.LayoutParams grow = new LinearLayout.LayoutParams(
	                        LayoutParams.MATCH_PARENT,
	                        0, 
	                        1.0f);
					
					LinearLayout.LayoutParams grow_body = new LinearLayout.LayoutParams(
	                        LayoutParams.MATCH_PARENT,
	                        LayoutParams.WRAP_CONTENT, 0.2f);
					

					
					exploreSearchBody.setLayoutParams(grow_body);
					
					cityLayout.setLayoutParams(grow);
					cuisineLayout.setLayoutParams(grow);
					goButtonLayout.setLayoutParams(grow);
					//spinnerTopPadding.setLayoutParams(grow_trans);
					exploreHasFocus = true;
				}
			}
        });
        
        topPicksPager = (TopPicksViewPager) findViewById(R.id.top_picks);
        topPicksPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        topPicksPager.setAdapter(topPicksPagerAdapter);
        
        homeButton = (ImageButton) findViewById(R.id.home_button);
        homeButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
		        
			}
        });
        
        setCountryButton();
        
        goButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
		        Intent intent = new Intent(HomePage.this, ResultsPage.class);
	        	int id = countryOISL.getCurrentCityId(citySpinner.getSelectedItemPosition());
	        	
	        	String[] cuisines = getResources().getStringArray(R.array.cuisine_entries);
	        	String[] cuisineIds = getResources().getStringArray(R.array.cuisine_ids);
	        	String cityName = citySpinner.getSelectedItem().toString();

		        if(exploreHasFocus){
		        	
		        	String area = areaSpinner.getSelectedItem().toString();
		        	String cuisine = cuisineSpinner.getSelectedItem().toString();
		        	int index = Arrays.binarySearch(cuisines, cuisine);
		        	String cuisineId;
		        	if (index >= 0)
		        		cuisineId = cuisineIds[index];
		        	else
		        		cuisineId = "-1";
		        	String info[] = {cityName, area, cuisineId, cuisine};
		        	intent.putExtra("info", info);
		        	
		        }
		        else{
		        	
		        	String searchTerms = searchBar.getText().toString();
	        		String info[] = {cityName, searchTerms};
	        		intent.putExtra("info", info);     		
		        }
		        if(exploreHasFocus){
		        	intent.putExtra("cityId",id);
		        	intent.putExtra("isExplore", exploreHasFocus);
	
			        startActivity(intent);
		        }
		        else
		        	Toast.makeText(getBaseContext(), "Haven't implemented Search yet!", Toast.LENGTH_SHORT).show();
			}
        });
        
        topPicksPager.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent resInfoPage = new Intent(HomePage.this, RestaurantInfoPage.class);
				int restaurantId = 62;
				switch(topPicksPager.getCurrentItem()){
				case 0:
					restaurantId = 993;
					break;
				case 1:
					restaurantId = 135;
					break;
				case 2:
					restaurantId = 371;
				}
				resInfoPage.putExtra("restaurant Id", restaurantId);
				
				startActivity(resInfoPage);
			}
        	
        });
        
 	}
    
    @Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
    	if(setSearchDefault){
	        searchButton.performClick();
	        setSearchDefault = false;
    	}

		super.onWindowFocusChanged(hasFocus);
	}

	private void setCountryButton(){
    	countrySpinner = (Spinner) findViewById(R.id.country_spinner);
        final String[] countryAbbrev = getResources().getStringArray(R.array.country_abbrev);
        final String[] countryNames = getResources().getStringArray(R.array.country_entries);
        countryOISL = new CountrySpinnerOnItemSelectedListener(getBaseContext(), countryAbbrev, countryNames, countryButton, citySpinner);
        int countryPos = countryPrefs.getInt(COUNTRY_POSITION, KENYA_DEFAULT_COUNTRY);
        countrySpinner.setSelection(countryPos);
        countrySpinner.setOnItemSelectedListener(countryOISL);
        Intent newCountryIntent = getIntent();
        String newCountry = newCountryIntent.getStringExtra(NEW_COUNTRY);
        if(newCountry != null)
        	countryOISL.setCitySpinner(newCountry);
        else
        	countryOISL.setCitySpinner(countryNames[countryPos]);
        countryButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
		    	new AlertDialog.Builder(HomePage.this)
		    	.setTitle("Change Country Preferences")
		    	.setMessage("Do you want to switch countries?")
		    	.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    	    public void onClick(DialogInterface dialog, int whichButton) {
		    	        countrySpinner.performClick();
		    	    }})
		    	 .setNegativeButton(android.R.string.no, null).show();
			}
        });
    }

    @Override
    protected void onPause(){
    	super.onPause();
    	if(topPicksPager.h != null)
    		topPicksPager.h.removeCallbacks(topPicksPager.animateViewPager);
    	
    }
    
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		exploreSearchBody.setBackgroundColor(getResources().getColor(R.color.transparent_eatout_orange));
		if(!exploreHasFocus){
			searchButton.setBackgroundColor(getResources().getColor(R.color.transparent_eatout_orange));
			exploreButton.setBackgroundColor(getResources().getColor(R.color.transparent_black));
		}
		else
			exploreButton.setBackgroundColor(getResources().getColor(R.color.transparent_eatout_orange));
			searchButton.setBackgroundColor(getResources().getColor(R.color.transparent_black));
		topPicksPager.h.removeCallbacks(topPicksPager.animateViewPager);
		topPicksPager.h.postDelayed(topPicksPager.animateViewPager, topPicksPager.ANIM_VIEWPAGER_DELAY);
		int countryPos = countryPrefs.getInt(COUNTRY_POSITION, KENYA_DEFAULT_COUNTRY);
		setCountryButton();
        countrySpinner.setSelection(countryPos);
		
	}
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_page, menu);
        return true;
    }
    
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter implements OnGestureListener {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	if(position >= NUM_PAGES)
        		position = 0;
        	
            return ScreenSlideFragment.create(position);
        }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return NUM_PAGES;
		}

		@Override
		public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
				float arg3) {
			// TODO Auto-generated method stub
			onFling(arg0, arg1, arg2, arg3);

			return true;
		}

		@Override
		public boolean onDown(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
				float arg3) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onLongPress(MotionEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onShowPress(MotionEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean onSingleTapUp(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return false;
		}

    }
    /************TEMPORARY**************************/
    /************TEMPORARY**************************/
    /************TEMPORARY**************************/
    /*public void runFirstTime() {
 	   
 	   DatabaseHandler db = new DatabaseHandler(this);
 	   String arr[] = {"African","American"};
 	   
 	   Log.d("Insert: ", "Inserting..");
 	   db.addRestaurant(new RestaurantEntry(174, "1The Horseman","Horseman", "Karen Shopping Centre, Langata Rd", false, "Kenya", arr, "Nairobi", "Karen", 4, 5, 5.0, 1.0, "fburl", "twitter", "html"));
 	   db.addRestaurant(new RestaurantEntry(175, "2The Horsemangggg","Horsebgbgbgngnman", "Karen Shdsgfafadsfhopping Centre, Langaadfasta Rd", false, "Kenya", arr, "Nairobi", "Kasdfdvvvaren", 4, 5, 5.0, 1.0, "fburl", "twitter", "html"));
 	   db.addRestaurant(new RestaurantEntry(176, "3The Horsemanasdfsaf","Horseman", "Karen Shopping Centre, Langata Rd", false, "Kenya", arr, "Nairobi", "Karen", 4, 5, 5.0, 1.0, "fbuvrl", "twitter", "html"));

    }*/
 }
/************TEMPORARY**************************/
/************TEMPORARY**************************/
/************TEMPORARY**************************/


    /*class UpdateSQLite extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
        	CitiesSQL citiesSQL = new CitiesSQL(getBaseContext());
        	
                for(int i=0;i<5;i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                return null;
        }        

        @Override
        protected void onPostExecute(String result) {             
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }*/
    
    
    /*private class CustomPager extends ViewPager implements OnGestureListener{
    	

		@Override
		public boolean onInterceptTouchEvent(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return super.onInterceptTouchEvent(arg0);
		}

		public CustomPager(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}

		@Override
		public boolean onDown(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean onFling(MotionEvent arg0, MotionEvent arg1, float velX,
				float velY) {
			// TODO Auto-generated method stub
			int nextPage = getCurrentItem()+1;
			if(nextPage >= NUM_PAGES)
				nextPage = 0;
			setCurrentItem(nextPage, true);
			
			
			return true;
		}

		@Override
		public void onLongPress(MotionEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
				float arg3) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onShowPress(MotionEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean onSingleTapUp(MotionEvent arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		
    }
    /*
    FOR ADJUSTING SMOOTHNESS OF THE VIEWPAGER onscrollchanged, setondraglistener?
    
    */
    


