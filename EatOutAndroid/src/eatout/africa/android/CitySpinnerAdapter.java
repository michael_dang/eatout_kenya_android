package eatout.africa.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class CitySpinnerAdapter extends ArrayAdapter<City> {
	    // Your sent context
	    private Context context;
	    // Your custom values for the spinner (User)
	    private City[] values;
	    private static LayoutInflater inflater=null;

	    public CitySpinnerAdapter(Context context, int textViewResourceId,
	            City[] values) {
	        super(context, textViewResourceId, values);
	        this.context = context;
	        this.values = values;
	        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	    }

	    public int getCount(){
	       return values.length;
	    }

	    public City getItem(int position){
	       return values[position];
	    }

	    public long getItemId(int position){
	       return position;
	    }


	    // And the "magic" goes here
	    // This is for the "passive" state of the spinner
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
	    	View vi=convertView;
	        if(convertView==null)
	            vi = inflater.inflate(R.style.spinner_style, null);
	        // Then you can get the current item using the values array (Users array) and the current position
	        // You can NOW reference each method you has created in your bean object (User class)
	        //convertView.set

	        // And finally return your dynamic (or custom) view for each spinner item
	        return vi;
	    }

	    // And here is when the "chooser" is popped up
	    // Normally is the same view, but you can customize it if you want
	    /*@Override
	    public View getDropDownView(int position, View convertView,
	            ViewGroup parent) {
	        TextView label = new TextView(context);
	        label.setTextColor(Color.BLACK);
	        label.setText(values[position].getName());

	        return label;
	    }*/
	}