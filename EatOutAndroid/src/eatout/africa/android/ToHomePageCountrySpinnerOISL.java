package eatout.africa.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;

public class ToHomePageCountrySpinnerOISL implements OnItemSelectedListener  {
	private final String COUNTRY_POSITION = "country position";
	private final String COUNTRY_PREFERENCES = "country preferences";
	
	private Context context;
    private SharedPreferences pref;
    private String[] countryAbb;
    private String[] countryNames;
    private Button countryButton;
    private int currentCountryIndex;
    private boolean oncreate;

    public ToHomePageCountrySpinnerOISL(Context con, String[] country_abbrev, String[] country_names, Button countryBut) {
        context = con;
        oncreate = true;
    	pref = context.getSharedPreferences(COUNTRY_PREFERENCES, 0);
        countryAbb = country_abbrev;
        countryNames = country_names;
        countryButton = countryBut;
        currentCountryIndex = 0;
        for(int i = 0; i < countryAbb.length; i++){
        	if(countryAbb[i].equals(countryButton.getText())){
        		currentCountryIndex = i;
        		break;
        	}
        }
    }
    
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		if(!oncreate){
			if(!countryAbb[position].equals(countryAbb[currentCountryIndex]))
			{
				Intent toHomePage = new Intent(context, HomePage.class);
				toHomePage.putExtra("new country", countryNames[position]);
				toHomePage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(toHomePage);
			}
	    	pref.edit().putInt(COUNTRY_POSITION, position).commit();
		}
		oncreate = false;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}