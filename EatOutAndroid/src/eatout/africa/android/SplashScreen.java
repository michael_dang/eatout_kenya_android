package eatout.africa.android;

import java.io.IOException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;

public class SplashScreen extends Activity {

	private static final String FIRST_TIME = "first run preferences";
	private SharedPreferences firstTimePreferences;
	private ProgressBar progress;
	private boolean firstTime;
	private Button selectCountry;
	private DatabaseHandler db;
	private Spinner countrySpinner;
	private FromSplashToHomeOISL countryOISL;
	private boolean asyncDone;
	private boolean userPicked;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		asyncDone = false;
		userPicked = false;
		setContentView(R.layout.activity_splash_screen);
        firstTimePreferences = getSharedPreferences(FIRST_TIME, 0);
        firstTime = firstTimePreferences.getBoolean("first run", true);
        firstTime = true;
        selectCountry = (Button) findViewById(R.id.select_country);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        if(firstTime){
        	SharedPreferences.Editor editor = firstTimePreferences.edit();
        	editor.putBoolean("first run", false);
        	editor.commit();
        	final String[] countryNames = getResources().getStringArray(
    				R.array.country_entries);
        	
        	/*from resultspage:
        			int countryPos = countryPrefs.getInt(COUNTRY_POSITION,
				KENYA_DEFAULT_COUNTRY);
		final String[] countryAbbrev = getResources().getStringArray(
				R.array.country_abbrev);
		final String[] countryNames = getResources().getStringArray(
				R.array.country_entries);

		countrySpinner.setSelection(countryPos);
		countryButton.setText(countryAbbrev[countryPos]);
		countrySpinner
				.setOnItemSelectedListener(new ToHomePageCountrySpinnerOISL(
						getBaseContext(), countryAbbrev, countryNames,
						countryButton));
						*/
        	
        	MakeDatabase makeDatabase = new MakeDatabase();
        	makeDatabase.execute();
        	countryOISL = new FromSplashToHomeOISL(getBaseContext(), countryNames);
        	countrySpinner = (Spinner) findViewById(R.id.country_spinner);
            countrySpinner.setOnItemSelectedListener(countryOISL);
        	selectCountry.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) { // need to set userPicked to true and we need to check if asyncDone true. Do in OnItemSelectedListener?
					// TODO Auto-generated method stub
					countrySpinner.performClick();
					userPicked = true;
						
				}
        	});
        }
        else{
        	selectCountry.setVisibility(View.GONE);
			progress.setVisibility(View.VISIBLE);
			Intent homePage = new Intent(SplashScreen.this, HomePage.class);
			startActivity(homePage);
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	private class MakeDatabase extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			selectCountry.setVisibility(View.VISIBLE);
			Log.d("database", "finished");
			if(userPicked){
				Intent toHomePage = new Intent(SplashScreen.this, HomePage.class);
				startActivity(toHomePage);
			}
			

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			
			db = new DatabaseHandler(getBaseContext());
			try {
				db.makeDatabase();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return null;
		}
		
	}
	
}
